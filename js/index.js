//var brush2 = $('#draw_panel').add("div");
 /*
  brush2 = document.createElement('div');
  brush2.style.width = '20px';
  brush2.style.height = '20px';
  brush2.style.backgroundColor = 'red';
  document.getElementById('draw_panel2').appendChild(brush2);
 */

  //brush2.css("width","20px");
  //brush2.css("height","20px");
  //brush2.css("background-color", "red");

//02.01.2016 SimplePicker

/*
 * Very simple jQuery Color Picker
 * https://github.com/tkrotoff/jquery-simplecolorpicker
 *
 * Copyright (C) 2012-2013 Tanguy Krotoff <tkrotoff@gmail.com>
 *
 * Licensed under the MIT license
 */

(function($) {
  'use strict';

  /**
   * Constructor.
   */
  //var SimpleColorPicker = function(select, options) {
	var SimplePicker = function(select, options) {
    this.init('simplepicker', select, options);
  };

  /**
   * SimplePicker class.
   */
  SimplePicker.prototype = {
    constructor: SimplePicker,

    init: function(type, select, options) {
      var self = this;

      self.type = type;

      self.$select = $(select);
      self.$select.hide();

      self.options = $.extend({}, $.fn.simplepicker.defaults, options);

      self.$colorList = null;

      if (self.options.picker === true) {
        var selectText = self.$select.find('> option:selected').text();
        self.$icon = $('<span class="simplepicker icon"'
                     + ' title="' + selectText + '"'
                     + ' style="background: ' + self.$select.val() + ';"'
                     + ' role="button" tabindex="0">'
                     + '</span>').insertAfter(self.$select);
        self.$icon.on('click.' + self.type, $.proxy(self.showPicker, self));

        self.$picker = $('<span class="simplepicker picker ' + self.options.theme + '"></span>').appendTo(document.body);
        self.$colorList = self.$picker;

        // Hide picker when clicking outside
        $(document).on('mousedown.' + self.type, $.proxy(self.hidePicker, self));
        self.$picker.on('mousedown.' + self.type, $.proxy(self.mousedown, self));
      } else {
        self.$inline = $('<span class="simplepicker inline ' + self.options.theme + '"></span>').insertAfter(self.$select);
        self.$colorList = self.$inline;
      }

      // Build the list of colors
      // <span class="color selected" title="Green" style="background-color: #7bd148;" role="button"></span>
      self.$select.find('> option').each(function() {
        var $option = $(this);
        var color = $option.val();

        var isSelected = $option.is(':selected');
        var isDisabled = $option.is(':disabled');

        var selected = '';
        if (isSelected === true) {
          selected = ' data-selected';
        }

        var disabled = '';
        if (isDisabled === true) {
          disabled = ' data-disabled';
        }

        var title = '';
        if (isDisabled === false) {
          title = ' title="' + $option.text() + '"';
        }

        var role = '';
        if (isDisabled === false) {
          role = ' role="button" tabindex="0"';
        }

        var $colorSpan = $('<span class="color"'
                         + title
                         + ' style="background: ' + color + ';"'
                         + ' data-color="' + color + '"'
                         + selected
                         + disabled
                         + role + '>'
                         + '</span>');

        self.$colorList.append($colorSpan);
        $colorSpan.on('click.' + self.type, $.proxy(self.colorSpanClicked, self));

        var $next = $option.next();
        if ($next.is('optgroup') === true) {
          // Vertical break, like hr
          self.$colorList.append('<span class="vr"></span>');
        }
      });
    },

    /**
     * Changes the selected color.
     *
     * @param color the hexadecimal color to select, ex: '#fbd75b'
     */
    selectColor: function(color) {
      var self = this;

      var $colorSpan = self.$colorList.find('> span.color').filter(function() {
        return $(this).data('color').toLowerCase() === color.toLowerCase();
      });

      if ($colorSpan.length > 0) {
        self.selectColorSpan($colorSpan);
      } else {
        console.error("The given color '" + color + "' could not be found");
      }
    },

    showPicker: function() {
      var pos = this.$icon.offset();
      this.$picker.css({
        // Remove some pixels to align the picker icon with the icons inside the dropdown
        left: pos.left - 6,
        top: pos.top + this.$icon.outerHeight()
      });

      this.$picker.show(this.options.pickerDelay);
    },

    hidePicker: function() {
      this.$picker.hide(this.options.pickerDelay);
    },

    /**
     * Selects the given span inside $colorList.
     *
     * The given span becomes the selected one.
     * It also changes the HTML select value, this will emit the 'change' event.
     */
    selectColorSpan: function($colorSpan) {
      var color = $colorSpan.data('color');
      var title = $colorSpan.prop('title');

      // Mark this span as the selected one
      $colorSpan.siblings().removeAttr('data-selected');
      $colorSpan.attr('data-selected', '');

      if (this.options.picker === true) {
        this.$icon.css('background', color);
        this.$icon.prop('title', title);
        this.hidePicker();
      }

      // Change HTML select value
      this.$select.val(color);
    },

    /**
     * The user clicked on a color inside $colorList.
     */
    colorSpanClicked: function(e) {
      // When a color is clicked, make it the new selected one (unless disabled)
      if ($(e.target).is('[data-disabled]') === false) {
        this.selectColorSpan($(e.target));
        this.$select.trigger('change');
      }
    },

    /**
     * Prevents the mousedown event from "eating" the click event.
     */
    mousedown: function(e) {
      e.stopPropagation();
      e.preventDefault();
    },

    destroy: function() {
      if (this.options.picker === true) {
        this.$icon.off('.' + this.type);
        this.$icon.remove();
        $(document).off('.' + this.type);
      }

      this.$colorList.off('.' + this.type);
      this.$colorList.remove();

      this.$select.removeData(this.type);
      this.$select.show();
    }
  };

  /**
   * Plugin definition.
   * How to use: $('#id').simplepicker()
   */
  $.fn.simplepicker = function(option) {
    var args = $.makeArray(arguments);
    args.shift();

    // For HTML element passed to the plugin
    return this.each(function() {
      var $this = $(this),
        data = $this.data('simplepicker'),
        options = typeof option === 'object' && option;
      if (data === undefined) {
        $this.data('simplepicker', (data = new SimplePicker(this, options)));
      }
      if (typeof option === 'string') {
        data[option].apply(data, args);
      }
    });
  };

  /**
   * Default options.
   */
  $.fn.simplepicker.defaults = {
    // No theme by default
    theme: '',

    // Show the picker or make it inline
    picker: false,

    // Animation delay in milliseconds
    pickerDelay: 0
  };

})(jQuery);

//02.01.2016 end of SimplePicker

myPlayer = videojs("my_video_1",
   {    
  
   }, function () {
       this.timecode({
           timeFormat: 'PAL'
       });
   });


var MyVideo = videojs( "my_video_1" , {}); 
//$('#test').text(MyVideo.el().getBoundingClientRect().width);

////////////////////27.12.2015//////////
///////////////////////////////////////
 
/////01.02.2016

//!!!!!!!!!!!!!!!!!
var controlShadow = MyVideo.controlBar.addChild("button", {
  "el": MyVideo.controlBar.createEl("button", {"text":"hello", "role":"button"})
});

//var myTest = document.createElement('div');
controlShadow.el().style.top = '-3px'; 
controlShadow.el().style.zIndex = '2';
controlShadow.el().style.opacity = '0.5';
//myTest.el().style.position = 'absolute';
controlShadow.el().style.height = '33px';
//controlShadow.el().style.backgroundColor = 'orange';
controlShadow.el().style.display = 'none';
//myTest9.addClass("mytest");
//myCtrl = MyVideo.controlBar.addChild(myTest, {}); 
//myCtrl.addClass("vjs-test");

////01.02.2016 end 


MyVideo.ready(function() {
  
  
/////////////////////////////////////////////////
////////////////////////////////////////////////
//////////////Markers functions/////////////////
  
  
  var defaultSetting = {
      markerStyle: {
         'width':'2px',
         //'border-radius': '30%',
         'background-color': 'white',
         //'height':'5px'
      },
      markerTip: {
         display: true,
         text: function(marker) {
            return "Break: "+ marker.text;
         },
         time: function(marker) {
            return marker.time;
         },
        image: function(marker) {
            return marker.image;
         },
        image2: function(marker) {
            return marker.image2;
         },
        image3: function(marker) {
            return marker.image3;
         },
        
      },
      breakOverlay:{
         display: false,
         displayTime: 3,
         text: function(marker) {
            return "Break overlay: " + marker.overlayText;
         },
         style: {
            'width':'100%',
            'height': '20%',
            'background-color': 'rgba(0,0,0,0.7)',
            'color': 'white',
            'font-size': '17px'
         }
      },
      onMarkerClick: function(marker) {
        
        if (!player.isFullscreen()) { 
         
        player.snap(marker.image2, marker.image3);
          
        $('.vjs-canvas-parent').show(); 
        
        $('#text_description').val(marker.text);  
            
        }

      },
      onMarkerReached: function(marker) {},
      markers: []
        
   };
   
   // create a non-colliding random number
   function generateUUID() {
      var d = new Date().getTime();
      var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
         var r = (d + Math.random()*16)%16 | 0;
         d = Math.floor(d/16);
         return (c=='x' ? r : (r&0x3|0x8)).toString(16);
      });
      return uuid;
   };
   
   function registerVideoJsMarkersPlugin(options) {
      /**
       * register the markers plugin (dependent on jquery)
       */
   
      var setting      = $.extend(true, {}, defaultSetting, options),
          markersMap   = {},
          markersList  = [], // list of markers sorted by time
          videoWrapper = $(this.el()),
          currentMarkerIndex  = -1, 
          player       = this,
          markerTip    = null,
          breakOverlay = null,
          overlayIndex = -1;
          
      function sortMarkersList() {
         // sort the list by time in asc order
         markersList.sort(function(a, b){
            return setting.markerTip.time(a) - setting.markerTip.time(b);
         });
      }
      
      function addMarkers(newMarkers) {
         // create the markers
         $.each(newMarkers, function(index, marker) {
            marker.key = generateUUID();
            
            videoWrapper.find('.vjs-progress-control').append(
               createMarkerDiv(marker));
            
            // store marker in an internal hash map
            markersMap[marker.key] = marker;
            markersList.push(marker);          
         });
         
         sortMarkersList();
      }
      
      function getPosition(marker){
         return (setting.markerTip.time(marker) / player.duration()) * 100
      }
      
      function createMarkerDiv(marker, duration) {
         var markerDiv = $("<div class='vjs-marker' style='z-index:1'></div>")
         markerDiv.css(setting.markerStyle)
            .css({"margin-left" : -parseFloat(markerDiv.css("width"))/2 + 'px', 
               "left" : getPosition(marker) + '%'})
            .attr("data-marker-key", marker.key)
            .attr("data-marker-time", setting.markerTip.time(marker));
        
         // add user-defined class to marker
         if (marker.class) {
            markerDiv.addClass(marker.class);
         }
         
         // bind click event to seek to marker time
         markerDiv.on('click', function(e) {
            //$('#test').text('hello2');
            var preventDefault = false;
            if (typeof setting.onMarkerClick === "function") {
               // if return false, prevent default behavior
               preventDefault = setting.onMarkerClick(marker) == false;
            }
            
            if (!preventDefault) {
               var key = $(this).data('marker-key');
               player.currentTime(setting.markerTip.time(markersMap[key]));
            }
         
         
         });
         
         if (setting.markerTip.display) {
            registerMarkerTipHandler(markerDiv);
         }
         
         return markerDiv;
      }      
      function updateMarkers() {
         // update UI for markers whose time changed

         for (var i = 0; i< markersList.length; i++) {
            var marker = markersList[i];
            var markerDiv = videoWrapper.find(".vjs-marker[data-marker-key='" + marker.key +"']"); 
            var markerTime = setting.markerTip.time(marker);
            
            if (markerDiv.data('marker-time') != markerTime) {
               markerDiv.css({"left": getPosition(marker) + '%'})
                  .attr("data-marker-time", markerTime);
            }
         }
         sortMarkersList();
      }
     

      function removeMarkers(indexArray) {
         // reset overlay
         if (breakOverlay){
             overlayIndex = -1;
             breakOverlay.css("visibility", "hidden");
         }
         currentMarkerIndex = -1;

         for (var i = 0; i < indexArray.length; i++) {
            var index = indexArray[i];
            var marker = markersList[index];
            if (marker) {
               // delete from memory
               delete markersMap[marker.key];
               markersList[index] = null;
               
               // delete from dom
               videoWrapper.find(".vjs-marker[data-marker-key='" + marker.key +"']").remove();
            }
         }
         
         // clean up array
         for (var i = markersList.length - 1; i >=0; i--) {
            if (markersList[i] === null) {
               markersList.splice(i, 1);
            }
         }
         
         // sort again
         sortMarkersList();
      }
      
      
      // attach hover event handler
      function registerMarkerTipHandler(markerDiv) {
         
         markerDiv.on('mouseover', function(){
            var marker = markersMap[$(this).data('marker-key')];
            //07.01.2016
            //markerTip.find('.vjs-tip-inner').text(setting.markerTip.text(marker));
           //07.01.2016 end        
           //05.01.2016           
           markerTip.find('.vjs-tip-image').attr('src', setting.markerTip.image(marker));
           
           //05.01.2016 end
            
            // margin-left needs to minus the padding length to align correctly with the marker
           if (MyVideo.el().getBoundingClientRect().width*(getPosition(marker)/100) >= parseFloat(markerTip.css("width"))/2) {
           
           if (MyVideo.el().getBoundingClientRect().width*((100-getPosition(marker))/100) < parseFloat(markerTip.css("width"))/2) {
           
             
              Mleft = -(MyVideo.el().getBoundingClientRect().width*((parseFloat(getPosition(marker)))/100))+parseFloat(MyVideo.el().getBoundingClientRect().width)-parseFloat(markerTip.css("width"))+6+'px';
             //$('#test').text(100-parseFloat(getPosition(marker)));
             //Mleft = '0px';
            
           //  
             
           Aleft = parseFloat(markerTip.css("width"))-parseFloat(MyVideo.el().getBoundingClientRect().width)*((100-getPosition(marker))/100)-7+'px';
            // $('#test').text(Aleft);
             markerTip.find('.vjs-tip-arrow').css('left', Aleft);  
             
           } else {  
             
              Mleft = -parseFloat(markerTip.css("width"))/2 + 'px';
            
              markerTip.find('.vjs-tip-arrow').css('left', '50%');  
           
           }  
             
           } else {
             
           Mleft = -(MyVideo.el().getBoundingClientRect().width*(getPosition(marker)/100)+6)+'px';  
            
           //  
             
           Aleft = MyVideo.el().getBoundingClientRect().width*(getPosition(marker)/100)+6+'px';
             
             markerTip.find('.vjs-tip-arrow').css('left', Aleft);
             
           };
           
           //markerTip.find('.vjs-tip-arrow').css('left', getPosition(marker)+'%');
           
            markerTip.css({
                            //"left" : getPosition(marker) + '%',
                            "left" : (MyVideo.el().getBoundingClientRect().width*(getPosition(marker)/100))+'px',
                            "margin-left" : Mleft,
                            //"margin-left" : -parseFloat(markerTip.css("width"))/2 + 'px',
                           "visibility"  : "visible"
                          });
            
         }).on('mouseout',function(){
            markerTip.css("visibility", "hidden");
         });
      }
      
      function initializeMarkerTip() {
         markerTip = $("<div class='vjs-tip'><div class='vjs-tip-arrow'></div><div class='vjs-tip-inner'></div><div><img class='vjs-tip-image' width='100' /></div></div>");
         videoWrapper.find('.vjs-progress-control').append(markerTip);
      }
      
      // show or hide break overlays
      function updateBreakOverlay() {
         if(!setting.breakOverlay.display || currentMarkerIndex < 0){
            return;
         }
         
         var currentTime = player.currentTime();
         var marker = markersList[currentMarkerIndex];
         var markerTime = setting.markerTip.time(marker);
         
         if (currentTime >= markerTime && 
            currentTime <= (markerTime + setting.breakOverlay.displayTime)) {
            if (overlayIndex != currentMarkerIndex){
               overlayIndex = currentMarkerIndex;
               breakOverlay.find('.vjs-break-overlay-text').text(setting.breakOverlay.text(marker));
            }
            
            breakOverlay.css('visibility', "visible");
            
         } else {
            overlayIndex = -1;
            breakOverlay.css("visibility", "hidden");
         }
      }
      
      // problem when the next marker is within the overlay display time from the previous marker
      function initializeOverlay() {
         breakOverlay = $("<div class='vjs-break-overlay'><div class='vjs-break-overlay-text'></div></div>")
            .css(setting.breakOverlay.style);
         videoWrapper.append(breakOverlay);
         overlayIndex = -1;
      }
      
      function onTimeUpdate() {
         onUpdateMarker();
         updateBreakOverlay();
      }
      
      function onUpdateMarker() {
         /*
             check marker reached in between markers
             the logic here is that it triggers a new marker reached event only if the player 
             enters a new marker range (e.g. from marker 1 to marker 2). Thus, if player is on marker 1 and user clicked on marker 1 again, no new reached event is triggered)
         */
         
         var getNextMarkerTime = function(index) {
            if (index < markersList.length - 1) {
               return setting.markerTip.time(markersList[index + 1]);
            } 
            // next marker time of last marker would be end of video time
            return player.duration();
         }
         var currentTime = player.currentTime();
         var newMarkerIndex;
         
         if (currentMarkerIndex != -1) {
            // check if staying at same marker
            var nextMarkerTime = getNextMarkerTime(currentMarkerIndex);
            if(currentTime >= setting.markerTip.time(markersList[currentMarkerIndex]) &&
               currentTime < nextMarkerTime) {
               return;
            }
            
            // check for ending (at the end current time equals player duration)
            if (currentMarkerIndex === markersList.length -1 &&
               currentTime === player.duration()) {
               return;
            }
         }
         
         // check first marker, no marker is selected
         if (markersList.length > 0 &&
            currentTime < setting.markerTip.time(markersList[0])) {
            newMarkerIndex = -1;
         } else {
            // look for new index
            for (var i = 0; i < markersList.length; i++) {
               nextMarkerTime = getNextMarkerTime(i);
               
               if(currentTime >= setting.markerTip.time(markersList[i]) &&
                  currentTime < nextMarkerTime) {
                  newMarkerIndex = i;
                  break;
               }
            }
         }
         
         // set new marker index
         if (newMarkerIndex != currentMarkerIndex) {
            // trigger event
            if (newMarkerIndex != -1 && options.onMarkerReached) {
              options.onMarkerReached(markersList[newMarkerIndex]);
            }
            currentMarkerIndex = newMarkerIndex;
         }
         
      }
      
      // setup the whole thing
      function initialize() {
         if (setting.markerTip.display) {
            initializeMarkerTip();
         }
      
         // remove existing markers if already initialized
         player.markers.removeAll();
         addMarkers(options.markers);
                  
         if (setting.breakOverlay.display) {
            initializeOverlay();
         }
         onTimeUpdate();
         player.on("timeupdate", onTimeUpdate);
      }
      
      // setup the plugin after we loaded video's meta data
      player.on("loadedmetadata", function() {
         initialize();
      });
      
      // exposed plugin API
      player.markers = {
         getMarkers: function() {
           return markersList;
         },
         next : function() {
            // go to the next marker from current timestamp
            var currentTime = player.currentTime();
            for (var i = 0; i < markersList.length; i++) {
               var markerTime = setting.markerTip.time(markersList[i]);
               if (markerTime > currentTime) {
                  player.currentTime(markerTime);
                  break;
               }
            }
         },
         prev : function() {
            // go to previous marker
            var currentTime = player.currentTime();
            for (var i = markersList.length - 1; i >=0 ; i--) {
               var markerTime = setting.markerTip.time(markersList[i]);
               // add a threshold
               if (markerTime + 0.5 < currentTime) {
                  player.currentTime(markerTime);
                  break;
               }
            }
         },
         add : function(newMarkers) {
            // add new markers given an array of index
            addMarkers(newMarkers);
         },
         remove: function(indexArray) {
            // remove markers given an array of index
            removeMarkers(indexArray);
         },
         removeAll: function(){
            var indexArray = [];
            for (var i = 0; i < markersList.length; i++) {
               indexArray.push(i);
            }
            removeMarkers(indexArray);
         },
         updateTime: function(){
            // notify the plugin to update the UI for changes in marker times 
            updateMarkers();
         },
         reset: function(newMarkers){
            // remove all the existing markers and add new ones
            player.markers.removeAll();
            addMarkers(newMarkers);
         },
         destroy: function(){
            // unregister the plugins and clean up even handlers
            player.markers.removeAll();
            breakOverlay.remove();
            markerTip.remove();
            player.off("timeupdate", updateBreakOverlay);
            delete player.markers;
         },
      };
   }

   videojs.plugin('markers', registerVideoJsMarkersPlugin);
  
 
/////////////////////////////////////////////////
////////////////////////////////////////////////
//////////////end of Markers functions//////////
MyVideo.markers({});  
  
 /*

///////////////!!!!!!! working example 26.12.2015

    var VjsButton = videojs.getComponent('Button');
    var MyNewButton = videojs.extend(VjsButton, {
       contructor: function(){
          
          VjsButton.call(this, player, options);
           this.on('click', this.onClick);
        },
      
    });
    
videojs.MyNewButton.prototype.handleClick = function() {
  
  MyVideo.play();
  
}

myButton = MyVideo.controlBar.addChild(new MyNewButton, {type:"button", value:"HD"});
myButton.addClass("vjs-test");

///////////////!!!!!!!end of working example 27.12.2015

*/

    var VjsButton = videojs.getComponent('Button');
    videojs.MyNewButton = videojs.extend(VjsButton, {
       contructor: function(){
          
          VjsButton.call(this, player, options);
           this.on('click', this.onClick());
         
        },
      
    });
  
videojs.MyNewButton.prototype.handleClick = function() {
  
  paint();
  /*
  
  MyVideo.markers.add([{
            time: MyVideo.currentTime(),
            //time: MillToTimecode(MyVideo.currentTime(), 'PAL'),
            text: $('#text_description').val()
         }]);
  $('#text_description').text('');
  var markers = MyVideo.markers.getMarkers();
  var minfo = '';
  
  if (markers.length>0) {
    
    for (var i = 0; i < markers.length; i++) {

      //minfo += "<div>"+markers[i].time+"   "+markers[i].text+"</div>";
      minfo += "<div>"+(i+1)+".   "+MillToTimecode(markers[i].time, 'PAL')+"   "+markers[i].text+"</div>";

      }
  

    $('#test').html(minfo);
    //$('#test').text(MillToTimecode(MyVideo.currentTime(), 'PAL'));
  
  }
  */
}
  
myButton = MyVideo.controlBar.addChild(new videojs.MyNewButton, {});

myButton.addClass("vjs-my-new-button");

  ///////////////!!!!!!!end of working example 27.12.2015
    
////////////////////
////////////////////
  
  
/////adding new buttonS!!!!/////////////
///////////////////////////////////////
var button_array = MyVideo.options().addButtons;
  
//$('#test').text('test:  '+button_array[1].name+"  "+button_array[1].content+"  "+button_array[1].click);  

////////////////////
/////////////////Class///////////////  

  
  for (ab = 0; ab < button_array.length; ab++) { 
   //var ab=0;
  
   var ClassButton = videojs.getComponent('Button');
    videojs.addButton = videojs.extend(ClassButton, {
       
        contructor: function(){
          
          ClassButton.call(this, player, options);
           this.on('click', this.onClick());
         
        },
      
    }); 
    
   /// 
   nb =  new videojs.addButton;
    
   addButton = MyVideo.controlBar.addChild(nb, {});

   addButton.addClass("vjs-ab-"+button_array[ab].name);

  $('head').append("<style>.vjs-ab-"+button_array[ab].name+"::after { content:'"+button_array[ab].content+"'; }</style>");  
  $(".vjs-ab-"+button_array[ab].name).css({"cursor":"pointer", "position":"absolute", "right":""+(2.5+8.5+ab*2.5)+"em"});  
  $(".vjs-ab-"+button_array[ab].name).attr('id', button_array[ab].click)
  //$(".vjs-ab-"+button_array[ab].name).text2(ab); 
  $(".vjs-ab-"+button_array[ab].name).click(function() {eval(this.id+'()')});  
    
 }
  

//////////////////end of Class////////
//////////////////  
});
/////end of adding new buttonS!!!!/////////////
///////////////////////////////////////


///////////////////end of 27.12.2015////
///////////////////////////////////////


/////////04.01.2016//////////////
////////////////////////////////
////////////////////////////////



/////////Snapshot functions///

var MyVideo = videojs( "my_video_1" , {});

/*
 $('body').keypress(function(e){
    //alert(e.which);
    if ((e.which == 27)){
        // Close my modal window
        $('.vjs-canvas-parent').show();
    }
});
*/

function snapshot(options) {

	var player = this;
	var video = player.el().querySelector('video');
	var container, scale;
  var x0, y0;
  var my_color = 'red';
  var my_size = 10;
  var my_tool = 'brush';
  var paint = false;
  var flag = false;
  var flag_fs = false;
  var flag_ns = false;
  var mim1;
  var mim2;
  //var context_bg;
  //var canvas_bg;
 
  
  function fullscreen_toggle () {
  
  //if (($('.vjs-canvas-parent').css("display") =='none') || ($('.vjs-canvas-parent').css("visibility") =='hidden')) {
  if (player.isFullscreen()) {
         
        if (flag_ns) {
           
            $('.vjs-canvas-parent').show();
            flag_fs = false;
            flag_ns = false;
            
          }
          
        } else {
          
          flag_fs = true;
          //$("#idElement").is(":visible") 
          if ($('.vjs-canvas-parent').is(":visible")) {
          
            $('.vjs-canvas-parent').hide();
          
            flag_ns = true;
            
          } else {
            
            //flag_ns = false;
            
          }  
          
        };
  
    
  
    
  };
  
  
   //////
    
        $(document).keyup(function(e) {
      
               
          if (e.keyCode == 27) { // escape key maps to keycode `27`
          // <DO YOUR WORK HERE>
                 
            if (flag_fs) {
            
            if (flag_ns) {  
              
              $('.vjs-canvas-parent').show();
              flag_ns = false;
            
            };
              
            flag_fs = false;

            }
           };
      
         });
    
          ///////
  
	function updateScale(){
		var rect = video.getBoundingClientRect();
		var scalew = canvas_draw.el().width / rect.width;
		var scaleh = canvas_draw.el().height / rect.height;
		scale = Math.max(Math.max(scalew, scaleh), 1);
		scale_txt.el().innerHTML = (Math.round(1/scale*100)/100) +"x";
	};

	player.snap = function(im1, im2){
    //paint = false;
    //im1 = typeof im1 !== 'undefined';
    //im2 = typeof im2 !== 'undefined';
    im1 = im1 || false;
    im2 = im2 || false;
     
    //player.controlBar.progressControl.seekBar.on("mousedown",  player.controlBar.progressControl.seekBar.handleMouseDown);
    
		player.pause();
		// loose keyboard focus
		player.el().blur();
		// switch control bar to drawing controls
    
		///31.01.2016
    //player.controlBar.hide();
    ///end 31.01.2016
    
		/*drawCtrl.show();*/
		// display canvas
		parent.show();
    
    
		// canvas for drawing, it's separate from snapshot because of delete
		canvas_draw.el().width = video.videoWidth;
		canvas_draw.el().height = video.videoHeight;
		//context_draw.strokeStyle = color.el().value;
    context_draw.strokeStyle = my_color;
		//context_draw.lineWidth = size.el().value / 2;
    context_draw.lineWidth = my_size / 2;
		context_draw.lineCap = "round";
		// calculate scale
		updateScale();

		// background canvas containing snapshot from video
    
		canvas_bg.el().width = video.videoWidth;
		canvas_bg.el().height = video.videoHeight;
    
    //////////06.02.2016
    /*
    if (player.fullWindowOnEscKey()) {
      
      $('.vjs-canvas-parent').show();
      
    };
    */
    /*
    {
    if (event.keyCode === 27) {
      if (this.isFullscreen() === true) {
        this.exitFullscreen();
      //} else {
      //  this.exitFullWindow();
      }
    } 
  } 
  */
    
    ///////
    
    player.controlBar.progressControl.seekBar.on("mousedown",  function () {
      
      $('.vjs-canvas-parent').hide();
      flag = false;
      player.controlBar.progressControl.seekBar.handleMouseDown;
                                                                           });
  player.controlBar.progressControl.seekBar.on("touchstart", function () {
    
    $('.vjs-canvas-parent').hide();
    flag = false;
    player.controlBar.progressControl.seekBar.handleMouseDown; 
                                                                         });
  player.controlBar.progressControl.seekBar.on("click", function () {
    
    $('.vjs-canvas-parent').hide();
    flag = false;
    player.controlBar.progressControl.seekBar.handleClick;
    
                                                                    
                                                                    });
    
    ////////
    
    
    player.controlBar.playToggle.on("mousedown",  function() {
      
        player.controlBar.playToggle.handleMouseDown;
        //$("#test").text("test1");
        //container.el().style.display = 'none';
        $('.vjs-canvas-parent').hide();
        flag = false;
      
      });
      
      //player.controlBar.fullscreenToggle.off("mousedown");
      
      player.controlBar.fullscreenToggle.on("mousedown",  function() {
        
        fullscreen_toggle();
        
         /*if (player.isFullscreen()!=true) {
          
          $('.vjs-canvas-parent').show();
          
        } else {
          
          $('.vjs-canvas-parent').hide();
          
        };*/
        
        
        player.controlBar.fullscreenToggle.handleMouseDown;
        //$("#test").text("test2");
        //container.el().style.display = 'none';
        
      
      });
    
    /////////06.02.2016 end
    
    if ((im1!=false) && (im2!=false)) {
      
      paint = false;
      flag = true;
      mim1 = im1;
      mim2 = im2;
      container.off("mousedown");
      //container.off('mouseup');
	    //container.off('mouseleave'); 
      container.el().style.cursor = 'default';
      //container.el().style.zIndex = '-1';
      $(".vjs-control-bar").css("z-index","100");
    
      
      //canvas_bg.el().src = im1;
      //canvas_draw.el().src = im2;
      //context_bg = canvas_bg.getContext("2d");
      /*
      
      var image = new Image();
      image.onload = function() {
      ctx.drawImage(image, 0, 0);
      };
      image.src =
      
      */
      
      var image01 = new Image();
      image01.onload = function() {
      context_bg.drawImage(image01, 0, 0);
      };
      
      image01.src = im1;
      
      //
      
       var image02 = new Image();
      image02.onload = function() {
      context_draw.drawImage(image02, 0, 0);
      };
      
      image02.src = im2;
      
      //context_bg.drawImage(im1, 0, 0);
      
      //context_draw = canvas_draw.getContext("2d");
      //context_draw.drawImage(im2, 0, 0);
      
      
    } else {
      
      //$('#test22').text('test5');
      
      if (flag && !paint) {
        
        //$('#test22').text('test3');
        //paint = true;
        flag = false; 
        //context_bg.drawImage(image01, 0, 0);
        //context_draw.drawImage(image02, 0, 0);
        var image01 = new Image();
      image01.onload = function() {
      context_bg.drawImage(image01, 0, 0);
      };
      
      image01.src = mim1;
      
      //
      
       var image02 = new Image();
      image02.onload = function() {
      context_draw.drawImage(image02, 0, 0);
      };
      
      image02.src = mim2;
      
        
      } else {
        
         //$('#test22').text('test4');
         context_bg.drawImage(video, 0, 0);
         
        
          
      };
      
      //context_bg.drawImage(video, 0, 0);
      //paint = true;
      
      container.on("mousedown", function(e){ forMouseDown(e); });
      //container.on('mouseup', finish);
	    //container.on('mouseleave', finish); 
      container.el().style.cursor = 'crosshair';
      //container.el().style.zIndex = '-1';
      $(".vjs-control-bar").css("z-index","1");
      
    };
    
		//context_bg.drawImage(video, 0, 0);
    
		// still fit into player element
		var rect = video.getBoundingClientRect(); // use bounding rect instead of player.width/height because of fullscreen
		canvas_draw.el().style.maxWidth  = rect.width  +"px";
		canvas_draw.el().style.maxHeight = rect.height +"px";
		canvas_bg.el().style.maxWidth  = rect.width  +"px";
		canvas_bg.el().style.maxHeight = rect.height +"px";
	
  
  //////////////
    
    parent.el().style.position = 'absolute'; 
    //parent.el().style.top='-'+(canvas_bg.el().height-140)+'px';
    parent.el().style.top ='0px';
    parent.el().style.marginBottom = '0px';
    parent.el().style.paddingBottom = '0px';
    parent.el().style.outline = '0px';
    parent.el().style.border = '0px';
    parent.el().style.height = (rect.height-1) +"px";
    
    //parent.el().style.backgroundColor = 'green';
    //container.el().style.zIndex='300';
    drawCtrl.el().style.position ='relative';
    drawCtrl.el().style.top ='0px';
    
    //////////////
    
    
  };
	// camera icon on normal player control bar
	var snap_btn = player.controlBar.addChild('button');
	snap_btn.addClass("vjs-snapshot-button");
	snap_btn.el().title = "Take snapshot";
	snap_btn.on('click', player.snap);

	// drawing controls

	// add canvas parent container before draw control bar, so bar gets on top
	var my_component = videojs.getComponent('Component');
  var parent = player.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl(null, {
				className: 'vjs-canvas-parent' /*TODO*/
			}),
		}) 
	);
 // parent.addClass("vjs-canvas-parent");
  
	//draw control bar
	
  var drawCtrl = player.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl(null, {
				className: 'vjs-control-bar vjs-drawing-ctrl',
			}),
		})
	);

   //var drawCtrl = $('#draw_panel');
       //drawCtrl.className("vjs-control-bar vjs-drawing-ctrl");
  
  // 31.01.2016
  /*
  var drawCtrl = document.getElementById('draw_panel').addChild(
		new my_component(player, {
			el: my_component.prototype.createEl(null, {
				className: 'vjs-control-bar vjs-drawing-ctrl',
			}),
		})
	);
  */
  // 31.01.2016 
  
	drawCtrl.hide();

	// choose color, used everywhere: painting, border color of cropbox, ...
	var color = drawCtrl.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl('input', {
				className: 'vjs-control', type: 'color', value: '#df4b26', title: 'color'
			}),
		})
	);
  
  //color.addClass("vjs-control");  
  color.el().id = 'colorChange';
	color.on('change', function(e){
		//context_draw.strokeStyle = color.el().value;
    context_draw.strokeStyle = my_color;
	});
  
 

	// choose size, used everywhere: line width, text size
	var size = drawCtrl.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl('input', {
				className: 'vjs-control', type: 'number', value: '10', title: 'line width, text size, ...'
			}),
		})
	);
  //size.addClass("vjs-control"); 
  
	size.on('keydown', function(e){ // don't fire player shortcuts when size input has focus
		//e.stopPropagation();
	}); 
	size.on('change', function(e){
		//context_draw.lineWidth = size.el().value / 2;
    context_draw.lineWidth = my_size / 2;
	});

	var tool = 'brush';
	function toolChange(event){
		var active_tool = drawCtrl.el().querySelector('.vjs-tool-active');
		active_tool.classList.remove('vjs-tool-active');
		event.target.classList.add('vjs-tool-active');
		tool = event.target.dataset.value;
	} 
 
  var bbButton = videojs.getComponent('Button');
  
	//videojs.ToolButton = videojs.Button.extend({
  videojs.ToolButton = videojs.extend(bbButton, {
		//init: function(p, options) {
    constructor: function(p, options) {
			bbButton.call(this, p, options);

			this.addClass("vjs-drawing-"+ options.tool);
			this.el().dataset.value = options.tool;
			this.el().title = options.title;

			this.on('click', toolChange);
		}
	});
	var brush  = drawCtrl.addChild(new videojs.ToolButton(player, {tool: "brush", title: "freehand drawing"}));
	brush.addClass("vjs-tool-active");
	var rect   = drawCtrl.addChild(new videojs.ToolButton(player, {tool: "rect",  title: "draw rectangle from top left to bottom right"}));
	
	var text   = drawCtrl.addChild(new videojs.ToolButton(player, {tool: "text",  title: "select area, type message and then click somewhere else"}));
	var eraser = drawCtrl.addChild(new videojs.ToolButton(player, {tool: "eraser",title: "erase drawing in clicked location"}));
  
  var arrow  = drawCtrl.addChild(new videojs.ToolButton(player, {tool: "arrow", title: "arrow drawing"}));
  
  var crop   = drawCtrl.addChild(new videojs.ToolButton(player, {tool: "crop",  title: "select area and click selection to crop"}));
  
  //arrow.addClass("vjs-tool-active");

	var scaler = drawCtrl.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl(null, {
				className: 'vjs-control vjs-drawing-scaler', title: 'scale image'
			})
		})
	);
  
  
  //////////31.01.2016/////////
  //add 1.02.2016
  color2 = document.createElement('div');
  color2.id = "color2";
  //color2.name = "colorpicker";
  //$('#draw_panel').css('padding-top', '10px');
  $('#draw_panel').append($(color2));
  
  //$("#color2").css("width","60px"); 
  //$("#color2").css("height","40px");
  //$("#brush2").css("background-color", "red");
  //$("#color2").css("background-color", "red");
  $("#color2").css("display", "inline-block");
  //$("#color2").css("cursor", "pointer");
  $("#color2").css("margin", "0px 5px 0px 5px");
  //$("#color2").css("border-radius", "12%"); 
  //$("#color2").css("padding-top","0px");
  //$("#color2").css("margin-top","0px");
  $("#color2").css("position","relative");
  $("#color2").css("top","-7px");  
  
  
  /////////////////
  sizePicker = document.createElement('select');
  sizePicker.id = "sizePicker";
  sizePicker.name = "sizepicker";
  //$('#draw_panel').css('padding-top', '10px');
  $("#draw_panel").append($(sizePicker));
  
  //$('#draw_panel').css('padding-top', '10px');
  $("#sizePicker").css("width","60px"); 
  $("#sizePicker").css("height","40px");
  $("#sizePicker").css("font-size","20px");
  //$("#brush2").css("background-color", "red");
  //$("#color2").css("background-color", "red");
  $("#sizePicker").css("display", "inline-block");
  //$("#color2").css("cursor", "pointer");
  $("#sizePicker").css("margin", "0px 5px 0px 5px");
  $("#sizePicker").css("border-radius", "12%"); 
  //$("#color2").css("padding-top","0px");
  //$("#color2").css("margin-top","0px");
  $("#sizePicker").css("position","relative");
  $("#sizePicker").css("top","5px");
  
  sizeOpt1 = document.createElement('option');
  sizeOpt1.text = '10';
  sizeOpt1.value = '10'; 
  sizePicker.add(sizeOpt1);
  
  sizeOpt2 = document.createElement('option');
  sizeOpt2.text = '20';
  sizeOpt2.value = '20'; 
  sizePicker.add(sizeOpt2);
  
  sizeOpt3 = document.createElement('option');
  sizeOpt3.text = '30';
  sizeOpt3.value = '30'; 
  sizePicker.add(sizeOpt3);
  
  sizeOpt4 = document.createElement('option');
  sizeOpt4.text = '40';
  sizeOpt4.value = '40'; 
  sizePicker.add(sizeOpt4);
  
  sizeOpt5 = document.createElement('option');
  sizeOpt5.text = '50';
  sizeOpt5.value = '50'; 
  sizePicker.add(sizeOpt5);
  
   //02.02.2016 
   // var my_color;
  my_size = $('select[name="sizepicker"]').val();
  var sp = $('select[name="sizepicker"]');
  sp.on('change', function() {
 //$('#colorChange').val($('select[name="colorpicker"]').val()); 
   //$('#colorChange').change();
    
   
   
   //$('#colorChange').val($('select[name="colorpicker"]').val());
   
   my_size = $('select[name="sizepicker"]').val();
   //context_draw.strokeStyle = my_size;
    context_draw.lineWidth = my_size / 2;
   //$("#test").text($('select[name="colorpicker"]').val());
   
});
  
  
  ///////////////
  tool2 = document.createElement('div');
  tool2.id = "tool2";
  //color2.name = "colorpicker";
  //$('#draw_panel').css('padding-top', '10px');
  $('#draw_panel').append($(tool2));
  
  //$("#color2").css("width","60px"); 
  //$("#color2").css("height","40px");
  //$("#brush2").css("background-color", "red");
  //$("#color2").css("background-color", "red");
  $("#tool2").css("display", "inline-block");
  //$("#color2").css("cursor", "pointer");
  $("#tool2").css("margin", "0px 5px 0px 5px");
  //$("#color2").css("border-radius", "12%"); 
  //$("#color2").css("padding-top","0px");
  //$("#color2").css("margin-top","0px");
  $("#tool2").css("position","relative");
  $("#tool2").css("top","-7px");  
  
  toolPicker = document.createElement('select');
  toolPicker.id = "toolPicker";
  toolPicker.name = "toolpicker";
  //$('#draw_panel').css('padding-top', '10px');
  $("#tool2").append($(toolPicker));
  
  //$('#draw_panel').css('padding-top', '10px');
  $("#toolPicker").css("width","40px"); 
  $("#toolPicker").css("height","40px");
  //$("#toolPicker").css("font-size","20px");
  //$("#brush2").css("background-color", "red");
  //$("#color2").css("background-color", "red");
  $("#toolPicker").css("display", "inline-block");
  //$("#color2").css("cursor", "pointer");
  $("#toolPicker").css("margin", "0px 5px 0px 5px");
  $("#toolPicker").css("border-radius", "12%"); 
  //$("#color2").css("padding-top","0px");
  //$("#color2").css("margin-top","0px");
  $("#toolPicker").css("position","relative");
  $("#toolPicker").css("top","-14px");
  
  
  
  toolOpt1 = document.createElement('option');
  toolOpt1.text = "brush";
  toolOpt1.value = "url('data:image/jpeg;base64,/9j/4QUWRXhpZgAATU0AKgAAAAgADAEAAAMAAAABAikAAAEBAAMAAAABAnIAAAECAAMAAAADAAAAngEGAAMAAAABAAIAAAESAAMAAAABAAEAAAEVAAMAAAABAAMAAAEaAAUAAAABAAAApAEbAAUAAAABAAAArAEoAAMAAAABAAIAAAExAAIAAAAcAAAAtAEyAAIAAAAUAAAA0IdpAAQAAAABAAAA5AAAARwACAAIAAgACvyAAAAnEAAK/IAAACcQQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzADIwMTY6MDI6MDEgMTg6MzQ6NDYAAASQAAAHAAAABDAyMjGgAQADAAAAAf//AACgAgAEAAAAAQAAACigAwAEAAAAAQAAACgAAAAAAAAABgEDAAMAAAABAAYAAAEaAAUAAAABAAABagEbAAUAAAABAAABcgEoAAMAAAABAAIAAAIBAAQAAAABAAABegICAAQAAAABAAADlAAAAAAAAABIAAAAAQAAAEgAAAAB/9j/7QAMQWRvYmVfQ00AAv/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIACgAKAMBIgACEQEDEQH/3QAEAAP/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APVUkkklKVPqnV+m9HxHZvUshmNjtIG9/cn81jG7nvd/JYqP1o+tfS/qzgnKzn7rXyMfGaR6ljv5P7rP37fzF559XumdY/xjdZ/bnX5b0XFeRTjCRW4jX7PT/I/7lX/n/wA3/wAUlPrGNk0ZePVlY7xZRextlVg4c1w3Md/mpKbGMYxrGNDWNADWgQABoGtCSSn/0PVVz/Wvrj0zByLOlYl9N/XHMP2fDe/a02R7Kbb49Kqx/wDg6nvZ6i0Ov5l2D0LqGbR/PY2NbbX/AFmMc9p/zl4h0r/F19cOtW1ZLsZ1FOURY7LyHAaO9xudXu+0P3fT/m/ekpvdC+rP1g+vP1kvyuuutrpxrNudY8bC0t/7Q49Z/m3f+ev5z/jPa8PExsLFqxMSttOPQ0MqraIAaFHBxG4WHTih7rfRrbWbXmXvLWhnqWO/Oe/ajpKUkkkkp//R9StqruqfTa0PrsaWPaeC1w2uaf7Kk1oa0NaIAEAeQXyskkp+qUl8rJJKfqlJfKySSn//2f/tDAJQaG90b3Nob3AgMy4wADhCSU0EBAAAAAAADxwBWgADGyVHHAIAAAIAAAA4QklNBCUAAAAAABDNz/p9qMe+CQVwdq6vBcNOOEJJTQQ6AAAAAACTAAAAEAAAAAEAAAAAAAtwcmludE91dHB1dAAAAAUAAAAAQ2xyU2VudW0AAAAAQ2xyUwAAAABSR0JDAAAAAEludGVlbnVtAAAAAEludGUAAAAAQ2xybQAAAABNcEJsYm9vbAEAAAAPcHJpbnRTaXh0ZWVuQml0Ym9vbAAAAAALcHJpbnRlck5hbWVURVhUAAAAAQAAADhCSU0EOwAAAAABsgAAABAAAAABAAAAAAAScHJpbnRPdXRwdXRPcHRpb25zAAAAEgAAAABDcHRuYm9vbAAAAAAAQ2xicmJvb2wAAAAAAFJnc01ib29sAAAAAABDcm5DYm9vbAAAAAAAQ250Q2Jvb2wAAAAAAExibHNib29sAAAAAABOZ3R2Ym9vbAAAAAAARW1sRGJvb2wAAAAAAEludHJib29sAAAAAABCY2tnT2JqYwAAAAEAAAAAAABSR0JDAAAAAwAAAABSZCAgZG91YkBv4AAAAAAAAAAAAEdybiBkb3ViQG/gAAAAAAAAAAAAQmwgIGRvdWJAb+AAAAAAAAAAAABCcmRUVW50RiNSbHQAAAAAAAAAAAAAAABCbGQgVW50RiNSbHQAAAAAAAAAAAAAAABSc2x0VW50RiNQeGxAUgAAAAAAAAAAAAp2ZWN0b3JEYXRhYm9vbAEAAAAAUGdQc2VudW0AAAAAUGdQcwAAAABQZ1BDAAAAAExlZnRVbnRGI1JsdAAAAAAAAAAAAAAAAFRvcCBVbnRGI1JsdAAAAAAAAAAAAAAAAFNjbCBVbnRGI1ByY0BZAAAAAAAAOEJJTQPtAAAAAAAQAEgAAAABAAIASAAAAAEAAjhCSU0EJgAAAAAADgAAAAAAAAAAAAA/gAAAOEJJTQQNAAAAAAAEAAAAHjhCSU0EGQAAAAAABAAAAB44QklNA/MAAAAAAAkAAAAAAAAAAAEAOEJJTScQAAAAAAAKAAEAAAAAAAAAAjhCSU0D9QAAAAAASAAvZmYAAQBsZmYABgAAAAAAAQAvZmYAAQChmZoABgAAAAAAAQAyAAAAAQBaAAAABgAAAAAAAQA1AAAAAQAtAAAABgAAAAAAAThCSU0D+AAAAAAAcAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAA4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAADaQAAAAYAAAAAAAAAAAAAACgAAAAoAAAAGgBwAGEAaQBuAHQAYgByAHUAcwBoAC0AcwB5AG0AYgBvAGwAXwAzADEAOAAtADkAMQA0ADUAAAABAAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAACgAAAAoAAAAAAAAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAEAAAAAEAAAAAAABudWxsAAAAAgAAAAZib3VuZHNPYmpjAAAAAQAAAAAAAFJjdDEAAAAEAAAAAFRvcCBsb25nAAAAAAAAAABMZWZ0bG9uZwAAAAAAAAAAQnRvbWxvbmcAAAAoAAAAAFJnaHRsb25nAAAAKAAAAAZzbGljZXNWbExzAAAAAU9iamMAAAABAAAAAAAFc2xpY2UAAAASAAAAB3NsaWNlSURsb25nAAAAAAAAAAdncm91cElEbG9uZwAAAAAAAAAGb3JpZ2luZW51bQAAAAxFU2xpY2VPcmlnaW4AAAANYXV0b0dlbmVyYXRlZAAAAABUeXBlZW51bQAAAApFU2xpY2VUeXBlAAAAAEltZyAAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAAAKAAAAABSZ2h0bG9uZwAAACgAAAADdXJsVEVYVAAAAAEAAAAAAABudWxsVEVYVAAAAAEAAAAAAABNc2dlVEVYVAAAAAEAAAAAAAZhbHRUYWdURVhUAAAAAQAAAAAADmNlbGxUZXh0SXNIVE1MYm9vbAEAAAAIY2VsbFRleHRURVhUAAAAAQAAAAAACWhvcnpBbGlnbmVudW0AAAAPRVNsaWNlSG9yekFsaWduAAAAB2RlZmF1bHQAAAAJdmVydEFsaWduZW51bQAAAA9FU2xpY2VWZXJ0QWxpZ24AAAAHZGVmYXVsdAAAAAtiZ0NvbG9yVHlwZWVudW0AAAARRVNsaWNlQkdDb2xvclR5cGUAAAAATm9uZQAAAAl0b3BPdXRzZXRsb25nAAAAAAAAAApsZWZ0T3V0c2V0bG9uZwAAAAAAAAAMYm90dG9tT3V0c2V0bG9uZwAAAAAAAAALcmlnaHRPdXRzZXRsb25nAAAAAAA4QklNBCgAAAAAAAwAAAACP/AAAAAAAAA4QklNBBEAAAAAAAEBADhCSU0EFAAAAAAABAAAAAE4QklNBAwAAAAAA7AAAAABAAAAKAAAACgAAAB4AAASwAAAA5QAGAAB/9j/7QAMQWRvYmVfQ00AAv/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIACgAKAMBIgACEQEDEQH/3QAEAAP/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APVUkkklKVPqnV+m9HxHZvUshmNjtIG9/cn81jG7nvd/JYqP1o+tfS/qzgnKzn7rXyMfGaR6ljv5P7rP37fzF559XumdY/xjdZ/bnX5b0XFeRTjCRW4jX7PT/I/7lX/n/wA3/wAUlPrGNk0ZePVlY7xZRextlVg4c1w3Md/mpKbGMYxrGNDWNADWgQABoGtCSSn/0PVVz/Wvrj0zByLOlYl9N/XHMP2fDe/a02R7Kbb49Kqx/wDg6nvZ6i0Ov5l2D0LqGbR/PY2NbbX/AFmMc9p/zl4h0r/F19cOtW1ZLsZ1FOURY7LyHAaO9xudXu+0P3fT/m/ekpvdC+rP1g+vP1kvyuuutrpxrNudY8bC0t/7Q49Z/m3f+ev5z/jPa8PExsLFqxMSttOPQ0MqraIAaFHBxG4WHTih7rfRrbWbXmXvLWhnqWO/Oe/ajpKUkkkkp//R9StqruqfTa0PrsaWPaeC1w2uaf7Kk1oa0NaIAEAeQXyskkp+qUl8rJJKfqlJfKySSn//2ThCSU0EIQAAAAAAVQAAAAEBAAAADwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAAABMAQQBkAG8AYgBlACAAUABoAG8AdABvAHMAaABvAHAAIABDAFMANQAAAAEAOEJJTQQGAAAAAAAHAAQAAAABAQD/4Q3caHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOmNycz0iaHR0cDovL25zLmFkb2JlLmNvbS9jYW1lcmEtcmF3LXNldHRpbmdzLzEuMC8iIHhtbG5zOnBob3Rvc2hvcD0iaHR0cDovL25zLmFkb2JlLmNvbS9waG90b3Nob3AvMS4wLyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczpkYz0iaHR0cDovL3B1cmwub3JnL2RjL2VsZW1lbnRzLzEuMS8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiBjcnM6QWxyZWFkeUFwcGxpZWQ9IlRydWUiIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHhtcDpDcmVhdGVEYXRlPSIyMDE2LTAyLTAxVDE4OjMwOjEzKzAyOjAwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxNi0wMi0wMVQxODozNDo0NiswMjowMCIgeG1wOk1ldGFkYXRhRGF0ZT0iMjAxNi0wMi0wMVQxODozNDo0NiswMjowMCIgZGM6Zm9ybWF0PSJpbWFnZS9qcGVnIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjI3NUJCRUFDMDFDOUU1MTE4Q0VCOEQwNUY5MzgwNTVDIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjI2NUJCRUFDMDFDOUU1MTE4Q0VCOEQwNUY5MzgwNTVDIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MjY1QkJFQUMwMUM5RTUxMThDRUI4RDA1RjkzODA1NUMiPiA8eG1wTU06SGlzdG9yeT4gPHJkZjpTZXE+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDoyNjVCQkVBQzAxQzlFNTExOENFQjhEMDVGOTM4MDU1QyIgc3RFdnQ6d2hlbj0iMjAxNi0wMi0wMVQxODozNDo0NiswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDxyZGY6bGkgc3RFdnQ6YWN0aW9uPSJzYXZlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDoyNzVCQkVBQzAxQzlFNTExOENFQjhEMDVGOTM4MDU1QyIgc3RFdnQ6d2hlbj0iMjAxNi0wMi0wMVQxODozNDo0NiswMjowMCIgc3RFdnQ6c29mdHdhcmVBZ2VudD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiBzdEV2dDpjaGFuZ2VkPSIvIi8+IDwvcmRmOlNlcT4gPC94bXBNTTpIaXN0b3J5PiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8P3hwYWNrZXQgZW5kPSJ3Ij8+/+4ADkFkb2JlAGQAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAEHBwcNDA0YEBAYFA4ODhQUDg4ODhQRDAwMDAwREQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAKAAoAwERAAIRAQMRAf/dAAQABf/EAaIAAAAHAQEBAQEAAAAAAAAAAAQFAwIGAQAHCAkKCwEAAgIDAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAACAQMDAgQCBgcDBAIGAnMBAgMRBAAFIRIxQVEGE2EicYEUMpGhBxWxQiPBUtHhMxZi8CRygvElQzRTkqKyY3PCNUQnk6OzNhdUZHTD0uIIJoMJChgZhJRFRqS0VtNVKBry4/PE1OT0ZXWFlaW1xdXl9WZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3OEhYaHiImKi4yNjo+Ck5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6voRAAICAQIDBQUEBQYECAMDbQEAAhEDBCESMUEFURNhIgZxgZEyobHwFMHR4SNCFVJicvEzJDRDghaSUyWiY7LCB3PSNeJEgxdUkwgJChgZJjZFGidkdFU38qOzwygp0+PzhJSktMTU5PRldYWVpbXF1eX1RlZmdoaWprbG1ub2R1dnd4eXp7fH1+f3OEhYaHiImKi4yNjo+DlJWWl5iZmpucnZ6fkqOkpaanqKmqq6ytrq+v/aAAwDAQACEQMRAD8A9U4q7FXYqlHmnzb5b8qaS+r+Yb+LTtPjIUzSk7seioihndj/ACorNiqO03UbHU9OttRsJluLG8iSe2nSvF45FDIwrQ7qcVf/0PVOKuxVhv5n/mt5W/LrQzqWsy+pcygrYabER69w47KD9lB+3K3wp/rcUZV88/l75Z84fn95z/xl53LR+TNMlK2empyWCRga/V4R3QbfWp/tv/d/8YlX1tDDFDEkMKLHFGoSONAFVVUUCqBsABir/9H1Tirz/wA5/nF5Z0bUJ/LOlX1ne+eHif8AR2jzSmKJ7ilUhlnp6UUj1/dxO6NJ9j4Oatir5e8jflp+YH5z/mPf6l51e5gs9Pn9LW55VMTRsh2sbeMikbD+Wn7pP3j/ABsvqKvtbSNI03R9MttL0y3S00+zjWG2toxRURRQAfxP7WKos4q//9L0X5/1i80XyN5g1eyFbzT9Ourm370kihZ1P0MK4q+H/Kv/ADjp+cHm65tdQk017Oy1NhcSavfyop4SnkZmjLG4ctXmP3fx/wDDYq+79D0lNI0ey01ZpLn6pBFA11MS0spiQJ6kjHdnfjVjiqPxV2Kv/9P1Jd2tvd2s1pcxiW2uEaKeJt1ZHBVlPsVOKqkaJGiog4ogCqo6ADYDFWzirWKtjFX/2Q==')";
  toolPicker.add(toolOpt1);
  
  toolOpt2 = document.createElement('option');
  toolOpt2.text = "rect";
  toolOpt2.value = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAYpJREFUeNrsmD9uwjAUhz8bG/on7CAWBELiCDBzBlYOwCE4DFyAiQnlJGwIxFCJhRSoo7hDmVBTQpNGEfWTvHjxp/f7Pfs9C2vtG/BMMeOogFfgqaCAUgIRxY1IUvAoPKCKMydQyolBACFQ/u5M9QO4zjFR4rISS2xzVtI+nAcBmE6n+L6PMQYhRDapshatNc1mk1arxWg0+j3gfD5nsVjgeR5RlM11KaXkdDpRrVbp9XrpADebDcPhkPF4zPl8zgSwUqmwXC6ZTCasVqt0Emut6Xa79Pv9TH213++/SjeBbeQtvxhjMjf+PWq4p84BOkAH6AAd4H8GFEKglMr8UK2TTxPq1qO+2+3YbrcEQZC6abXW4nke6/UagDAM0wHW63Vmsxm+72fasB4OB2q1Gu12Ox3gYDDAWpu4d0v0VRBFaK1pNBp0Op3b4561NgBervY/LnNqXmEuM/F1Tby7a+avAEXOHOLeIglzhjNxno8DLOecxXKcmnGAJVckjwRYZEipgIDifqQfPwcAV41xMDMzhIYAAAAASUVORK5CYII=')";
  toolPicker.add(toolOpt2);
  
  toolOpt3 = document.createElement('option');
  toolOpt3.text = "text";
  toolOpt3.value = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAIAAAADnC86AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAIGNIUk0AAHolAACAgwAA+f8AAIDpAAB1MAAA6mAAADqYAAAXb5JfxUYAAAGlSURBVHja7Fi7isJAFL0TYxIJtkoQFDQQbIKPSvwJa//BP7Gz9IesbQVtLFSwSWaMgzp3i2EXWTeRye4iyJwixdzcObmvnEkIIsIrYMCLoIk18b/BTDMIIXa73eVyIYTk2BcRi8Wi53lp7iRtjimlg8FgtVpZlpWDmHPebrcXi4XjOGoRA0AURZxzznm+ZFJK86QaAGSWSqVSpVK5T4xhGHEcH49HAHAcp1qt3lsJIfv9/nw+P6kRpiCO40ajAQDD4TBJEkpp/AlEnE6n0r3T6URRxBiTJsYYY6zf7wOA7/tJkqTtbz7NWKFQeKyTbdtf1nK5/OiSf5wQUQgBALfb7WnCHidCXjMUKDViy7LG4/F2uw3DULWtRqNREAT1et00TeUaZ2M2m0n3Xq8nI1OFfldrYk2siTXx+xMTQvKdBtWIhRBSnqXiSnG7Xq+ImC3bv5XF+XzebDa73W6tVpPuruuGYej7/mQyUdrKVHrKw+GwXq/vVxhjy+USADabzd8c6H+E53lBELiu+239dDq1Wi215lD9MM+4X6nLiP4joInfjvhjAEFwst1AWQz3AAAAAElFTkSuQmCC')";
  toolPicker.add(toolOpt3);
  
  toolOpt4 = document.createElement('option');
  toolOpt4.text = "eraser";
  toolOpt4.value = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABdtJREFUeNrsmFtI020cx387/Ec5FUubW5KG81BQUMNQRkGw2s00bMNC1JsSjbrpLqIDLcJOBAuWEdLFOhhFB6iLGSSmDgr8Q4iHIFNvmm22rfmfFjbX97148Xn9v5vO7d37sov3B/+L8TzP9//57/c8v8MjAeAmoixKTwvJiSiHiDLSFFAmJaIIpa9FpJTmlvaA8lQLzs/P0+vXr8npdNLExAQplUqqqKigXbt20bZt20ij0SQmCEBAiuzLly8wm80goqhHKpWisLAQ+/btw4ULF9DT0wOPx4NwOLySpJAyQJfLhdLSUga0du1a6HQ6lJSUxAQmIuTn56O6uhpWqxVOpxM+n+/fAWxvb0d2djZ7sV6vB8/zmJubQyAQAM/zuHHjBmpra1FWVgaFQhEFy3EcVCoVLBYLPn/+nBrAUCiE5uZm0Yuam5sxOzu77Jrv37/D5XLh1q1bqK+vR1FRETiOg0QiYRo6nQ7T09P/DHB0dBR6vV7k0vb29oR1IpEIeJ7Hjh07RB/65MmT5AGfPXuGDRs2MLGysjL09/cn5QVBEHDixAnI5XIRYGdnZ+KA4XAY586dg0wmY0IHDhyA2+1OCm5oaAgVFRVR+zEvLw+fPn1KDNDj8aCmpoaJyOVynD17Nl6YWNaePn0KtVrN9AoLC9nhaWxsTOyQ9Pb2ory8nImp1Wo8f/48KbBfv37h9OnTokPR2toKm83GfjscjtUD3r59G0qlki2urKzEyMhIUnBut1vkBY7jcO3aNQBAY2MjiAgajQZerzc+YDAYREtLi2hvtLS0IBgMJgXX29uL4uJikUu7uroAABMTE8zdhw4dih+oA4EA9u/fz8Ryc3Nx7969pAO53W5HRkYG0zMYDBgfH2fjjx49YmOPHz+OD3j06FHRP3f48GEMDg7i27dvCYHNzMzgyJEjTEcikeDkyZP4+fOnaN5i/i4oKMDk5OTKgK9evRJt4KXJvqSkBLW1tWhra4PL5UIoFFoWbnh4GFVVVWz9+vXrY3phYGAAWVlZsdwbG9BisbANXFlZic2bNy9bnWzcuBF1dXW4efMm+vr64PP54Pf74XA4oFKp2Nzt27eD5/kouOnpaZaNpFIp25PLAo6PjyM/P58l/Pn5ebjdbvT09KCtrQ1GoxFqtRocx0UBKxQKlJaWQqvVirJCQ0PD0lPJ7MOHD6Ig3draGr/cevjwIVtgs9lius7v96OrqwtWqxU1NTXsg/7+yGQyXL16Fb9//47S6OzsRF5eHptrMpkwMzMTH/DgwYMgIqxZswaDg4NxD8HCwgI8Hg/6+vpw5swZ5Obmspdevnw5av6PHz9w6tQpSKVSNu/48eMQBCF+wep2u1ksMhgMMb88XhBejHNVVVVYWFgQjU9OTsJoNDKwzMxMdHR0xK0lGODdu3fZ4kuXLiUc6168eMHW2+120Vh3dze0Wi0b37p1K1wu16qKHQIgRCIR1NfXM/eOjo4mDNjQ0AAiglKpxNDQEKv1rl+/LkqVZrN5sRhdPeDHjx+xbt06EBH27t0b5Z545vf7UVBQACKC0WhkqbKpqUlU/Vy8eDFR7T8B79+/z4SSqYqXnn673Y6xsTHodDpRc/Ty5cuk6lk5EZFc/ld7/ODBAxIEgfR6Pe3cuZMyMzPjtq5v3rwhIqLs7GwKBAK0e/du8nq9RES0Z88e6ujooPLy8uQabQCC1+uN6gkWM4XFYoHNZsPbt29j5uKvX79Co9EwNy7t2I4dO7ZiA7VqFwPA2NgYzGYzVCpVzEzBcRy0Wi1MJhOuXLmC9+/fY3Z2Fg6HI2puTk4O7ty5k4p2OzoX+3w+OJ1OWK1WVFdXL5spiAhFRUWifpiIsGXLFrx79y5VlxUrV9ThcBgejwfd3d04f/48DAYDNm3aJMoES5+6ujpMTU0hhSZIAAiJ3LBOTU3RyMgIDQwMEM/zNDc3R8XFxWQymchoNJJCoUjpDWvCgP/1FfD/F5ipAJSlMZ9MTkTBNL5ID/0xAHmUNSc28Vs8AAAAAElFTkSuQmCC')";
  toolPicker.add(toolOpt4);
  
  
  toolOpt5 = document.createElement('option');
  toolOpt5.text = "arrow";
  toolOpt5.value = "url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAoCAYAAACM/rhtAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAASZJREFUeNrs2MuNxCAMBmBY7Z20kApIB5SQEigBpQJKoYSUQDpAqSAlOB38e9rDalYzPDVzsCUuURR/EjYykQAgPji+xIcHA98KXJZFSCmfrnmexX3f9UlQGTFGCCGyVoyxNg24BhnIQAYykIEMZCADh8Z3zkvHcQgp5Z9n53lmJ9m2TUzT9DDseu8fnhcPrCml7MG0dKWUXg6sWRO11ro7zlqbNVFnAYkISqluOK01iKgf8HereyCVUllbWwwEgBBCMzCEUHRpKr7Vee+H110TEACstUPrrhlIREWdXVp3zUAAuK4ru2n2fa++uFcDcw9x51xLijbgq842xrR+vh0IAM65f+uupimGAAFgXdcuTTEMSEQwxkApVXwYPwvJ/6gZ+Ob4GQDn6jrKtoglJwAAAABJRU5ErkJggg==')";
  toolPicker.add(toolOpt5);
  //////////////  
  //02.02.2016 end
    
//02.02.2016 
   // var my_color;
  
  my_tool = $('select[name="toolpicker"] option:selected').text();
  var tp = $('select[name="toolpicker"]').simplepicker({picker: true, theme: 'glyphicons'});
  tp.on('change', function() {
 //$('#colorChange').val($('select[name="colorpicker"]').val()); 
   //$('#colorChange').change();
    
   
   
   //$('#colorChange').val($('select[name="colorpicker"]').val());
   
   my_tool = $('select[name="toolpicker"] option:selected').text();
   
   //$('#test').text(my_tool); 
   tool = my_tool;
   //context_draw.strokeStyle = my_color;
   //$("#test").text($('select[name="colorpicker"]').val());
   
});
//02.02.2016 end  
  
  
  colorPicker = document.createElement('select');
  colorPicker.id = "colorPicker";
  colorPicker.name = "colorpicker";
  
  $("#color2").append($(colorPicker));
  
  //$('#draw_panel').css('padding-top', '10px');
  //$("#color2").css("width","60px"); 
  //$("#color2").css("height","40px");
  //$("#brush2").css("background-color", "red");
  //$("#color2").css("background-color", "red");
  //$("#color2").css("display", "inline-block");
  //$("#color2").css("cursor", "pointer");
  //$("#color2").css("margin", "0px 5px 0px 5px");
  //$("#color2").css("border-radius", "12%"); 
  //$("#color2").css("padding-top","0px");
  //$("#color2").css("margin-top","0px");
  //$("#color2").css("top","0px");
  
  colorOpt1 = document.createElement('option');
  colorOpt1.text = 'Green';
  colorOpt1.value = '#7bd148'; 
  colorPicker.add(colorOpt1);
  
  colorOpt2 = document.createElement('option');
  colorOpt2.text = 'Bold blue';
  colorOpt2.value = '#5484ed'; 
  colorPicker.add(colorOpt2);
  
  colorOpt3 = document.createElement('option');
  colorOpt3.text = 'Blue';
  colorOpt3.value = '#a4bdfc'; 
  colorPicker.add(colorOpt3);
  
  colorOpt4 = document.createElement('option');
  colorOpt4.text = 'Turquoise';
  colorOpt4.value = '#46d6db'; 
  colorPicker.add(colorOpt4);
  
  colorOpt5 = document.createElement('option');
  colorOpt5.text = 'Light green';
  colorOpt5.value = '#7ae7bf'; 
  colorPicker.add(colorOpt5);
  
  colorOpt6 = document.createElement('option');
  colorOpt6.text = 'Bold green';
  colorOpt6.value = '#51b749'; 
  colorPicker.add(colorOpt6);
  
  colorOpt7 = document.createElement('option');
  colorOpt7.text = 'Yellow';
  colorOpt7.value = '#fbd75b'; 
  colorPicker.add(colorOpt7);
  
  colorOpt8 = document.createElement('option');
  colorOpt8.text = 'Orange';
  colorOpt8.value = '#ffb878'; 
  colorPicker.add(colorOpt8);
  
  colorOpt9 = document.createElement('option');
  colorOpt9.text = 'Red';
  colorOpt9.value = '#ff887c'; 
  colorPicker.add(colorOpt9);
  
  colorOpt10 = document.createElement('option');
  colorOpt10.text = 'Bold red';
  colorOpt10.value = '#dc2127'; 
  colorPicker.add(colorOpt10);
  
  colorOpt11 = document.createElement('option');
  colorOpt11.text = 'Purple';
  colorOpt11.value = '#dbadff'; 
  colorPicker.add(colorOpt11);
  
  
  colorOpt12 = document.createElement('option');
  colorOpt12.text = 'Gray';
  colorOpt12.value = '#e1e1e1'; 
  colorPicker.add(colorOpt12);
  
   //01.02.2016 
   // var my_color;
  
  my_color = $('select[name="colorpicker"]').val();
  var cp = $('select[name="colorpicker"]').simplecolorpicker({picker: true, theme: 'glyphicons'});
  cp.on('change', function() {
 //$('#colorChange').val($('select[name="colorpicker"]').val()); 
   //$('#colorChange').change();
    
   
   
   //$('#colorChange').val($('select[name="colorpicker"]').val());
   
   my_color = $('select[name="colorpicker"]').val();
   context_draw.strokeStyle = my_color;
   //$("#test").text($('select[name="colorpicker"]').val());
   
});
//01.02.2016 end

  /*
  <select name="colorpicker">
  <option value="#7bd148">Green</option>
  <option value="#5484ed">Bold blue</option>
  <option value="#a4bdfc">Blue</option>
  <option value="#46d6db">Turquoise</option>
  <option value="#7ae7bf">Light green</option>
  <option value="#51b749">Bold green</option>
  <option value="#fbd75b">Yellow</option>
  <option value="#ffb878">Orange</option>
  <option value="#ff887c">Red</option>
  <option value="#dc2127">Bold red</option>
  <option value="#dbadff">Purple</option>
  <option value="#e1e1e1">Gray</option>
</select>
  */
  
  /////////31.01.2016 end/////
  
	scaler.on('click', function(e){
		var w = canvas_draw.el().width, h = canvas_draw.el().height;
		var scalew = window.prompt("Current image size is "+w+"x"+h+" . New width?", w);
		scalew = parseInt(scalew, 10);
		if(!isNaN(scalew)){
			var factor = scalew / w;
			var width  = factor * w |0;
			var height = factor * h |0;

			var r = scaleCropCanvas(0, 0, w, h, width, height, canvas_bg, context_bg);
			canvas_bg = r[0]; context_bg = r[1];
			r = scaleCropCanvas(0, 0, w, h, width, height, canvas_draw, context_draw);
			canvas_draw = r[0]; context_draw = r[1];
			updateScale();
		}
		// just ignore
	});

	//player.comDr = function combineDrawing(encoding){
combineDrawing = function (encoding){
		//blit canvas and open new tab with image
		var canvas_tmp = document.createElement('canvas');
     
		canvas_tmp.width = canvas_draw.el().width;
		canvas_tmp.height = canvas_draw.el().height;
		var ctx_tmp = canvas_tmp.getContext("2d");
		ctx_tmp.drawImage(canvas_bg.el(), 0, 0);
		ctx_tmp.drawImage(canvas_draw.el(), 0, 0);
  
    //return canvas_tmp.toDataURL(encoding);  
  
    //06.01.2016
    var canvas_tmp2 = document.createElement('canvas');
  
    canvas_tmp2.width = canvas_draw.el().width;
		canvas_tmp2.height = canvas_draw.el().height;
		var ctx_tmp2 = canvas_tmp2.getContext("2d");
		ctx_tmp2.drawImage(canvas_bg.el(), 0, 0);
		//ctx_tmp2.drawImage(canvas_draw.el(), 0, 0);
  
   var canvas_tmp3 = document.createElement('canvas');
  
    canvas_tmp3.width = canvas_draw.el().width;
		canvas_tmp3.height = canvas_draw.el().height;
		var ctx_tmp3 = canvas_tmp3.getContext("2d");
		//ctx_tmp3.drawImage(canvas_bg.el(), 0, 0);
		ctx_tmp3.drawImage(canvas_draw.el(), 0, 0);
  
   //06.01.2016 end
  
  return [canvas_tmp2.toDataURL(encoding), canvas_tmp3.toDataURL(encoding), canvas_tmp.toDataURL(encoding)];
    
	}
/*
	var dljpeg = drawCtrl.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl(null, {
				className: 'vjs-control vjs-button', innerHTML: 'JPEG', title: 'open new tab with jpeg image'
			}),
		})
	);
  
	dljpeg.on('click', function(){ combineDrawing("image/jpeg"); });
  
	var dlpng = drawCtrl.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl(null, {
				className: 'vjs-control vjs-button', innerHTML: 'PNG', title: 'open new tab with png image'
			}),
		})                              
	);
  
	dlpng.on('click', function(){ combineDrawing("image/png"); });
*/
	// close button leading back to normal video play back
/*
	var close = drawCtrl.addChild('button');
	close.addClass("vjs-drawing-close");
	close.el().title = "close screenshot and return to video";
	close.on('click', function(){
    
    $('#paint_btn').show();
    $('#submit_btn').hide();
    $('#cancel_btn').hide();
    
		// hide all canvas stuff
		parent.hide();
		// switch back to normal player controls
		drawCtrl.hide();
		player.controlBar.show();
		player.el().focus();
	});
*/
	// scale display
	var scale_txt = drawCtrl.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl(null, {
				className: 'vjs-scale', innerHTML: '1', title: 'scale factor'
			}),
		})
	);
  scale_txt.addClass("vjs-scale");
	// canvas stuff
	container = parent.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl(null, {
				className: 'vjs-canvas-container' /*TODO*/
			}),
		})
	);
  container.addClass("vjs-canvas-container");
  
	//var canvas_bg = container.addChild( //FIXME: it's quite silly to use a component here
  var canvas_bg = container.addChild( //FIXME: it's quite silly to use a component here
		new my_component(player, {
			el: my_component.prototype.createEl('canvas', {
			}),
		})
	);
	//var context_bg = canvas_bg.el().getContext("2d");
  var context_bg = canvas_bg.el().getContext("2d");
	var canvas_draw = container.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl('canvas', {
			}),
		})
	);
	var context_draw = canvas_draw.el().getContext("2d");
  
  ///
  
  var canvas_arrow = container.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl('canvas', {
			}),
		})
	);
  canvas_arrow.el().style.zIndex = "2";
	var context_arrow = canvas_arrow.el().getContext("2d");
  
  ///
  
	var canvas_rect = container.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl('canvas', {
			}),
		})
	);
	canvas_rect.el().style.zIndex = "1"; // always on top of other canvas elements
	var context_rect = canvas_rect.el().getContext("2d");
	var cropbox = container.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl('div', {
				innerHTML: "crop"
			}),
		})
	);
	// crop handling, create new canvas and replace old one
	function scaleCropCanvas(left, top, width, height, newwidth, newheight, canvas, context){
// 		var newcanvas = document.createElement('canvas');
		var newcanvas = new my_component(player, { // FIXME: that's quite silly
			el: my_component.prototype.createEl('canvas', {
			}),
		});
		var rect = player.el().getBoundingClientRect();
		newcanvas.el().style.maxWidth  = rect.width  +"px";
		newcanvas.el().style.maxHeight = rect.height +"px";

		newcanvas.el().width = newwidth;
		newcanvas.el().height = newheight;
    
		var ctx = newcanvas.el().getContext("2d");
		ctx.drawImage(canvas.el(),
			left, top, width, height,
			0, 0, newwidth, newheight
		);

// 		container.replaceChild(newcanvas, canvas);
		container.removeChild(canvas);
		container.addChild(newcanvas);
// 		canvas = newcanvas;
		ctx.lineCap = context.lineCap; // transfer context states
		ctx.strokeStyle = context.strokeStyle;
		ctx.lineWidth = context.lineWidth;
		return [newcanvas, ctx];
	}
	cropbox.on('mousedown', function(e){
		var left   = scale * cropbox.el().offsetLeft  |0;
		var top    = scale * cropbox.el().offsetTop   |0;
		var width  = scale * cropbox.el().offsetWidth |0;
		var height = scale * cropbox.el().offsetHeight|0;
		var r = scaleCropCanvas(left, top, width, height, width, height, canvas_bg, context_bg);
		canvas_bg = r[0]; context_bg = r[1];
		r = scaleCropCanvas(left, top, width, height, width, height, canvas_draw, context_draw);
		canvas_draw = r[0]; context_draw = r[1];
		updateScale();

		cropbox.hide();
		e.stopPropagation(); //otherwise canvas below gets mousedown
	});

	var textbox = container.addChild(
		new my_component(player, {
			el: my_component.prototype.createEl('textarea', {
			}),
		})
	);
	textbox.on('keydown', function(e){ // don't fire player shortcuts when textbox has focus
		e.stopPropagation();
	});
	// draw text when textbox looses focus
	textbox.on('blur', function(e){
		//context_draw.fillStyle = color.el().value;
    context_draw.fillStyle = my_color;
		//context_draw.font = scale*size.el().value +"px sans-serif";
    context_draw.font = scale*my_size +"px sans-serif";
		context_draw.textBaseline = "top";
		context_draw.fillText(textbox.el().value,
				scale*textbox.el().offsetLeft + scale,
				scale*textbox.el().offsetTop + scale); //+1 for border?
		//FIXME: there's still a minor shift when scale isn't 1, in firefox more and also when scale is 1
		textbox.hide();
		textbox.el().value = "";
	});

	parent.hide();
	canvas_rect.hide();
  canvas_arrow.hide();
	cropbox.el().style.display = "none";
	textbox.hide();

	// TODO: draw functions
	//var paint = false;
function forMouseDown(e) {
  paint = true;
		var pos = container.el().getBoundingClientRect();
		var x = e.clientX - pos.left;
		var y = e.clientY - pos.top; 
		switch(tool){
			case "brush":
				x *= scale; y *= scale;
				context_draw.beginPath();
				context_draw.moveTo(x-1, y);
				context_draw.lineTo(x, y);
				context_draw.stroke();
				break;
      case "arrow":
        //x *= scale; y *= scale;
        x0 = x;
        y0 = y;
        
        canvas_arrow.el().width = video.getBoundingClientRect().width;
				canvas_arrow.el().height = video.getBoundingClientRect().height;
				canvas_arrow.el().style.left = canvas_draw.el().style.left;
				canvas_arrow.el().style.top = canvas_draw.el().style.top;
        //canvas_arrow.el().style.backgroundColor = 'green';
				canvas_arrow.show();
        
        //context_draw.save();
				//context_draw.lineTo(x, y);
				//context_draw.stroke();
       
				break; 
			case "rect":
				// rectangle is scaled when blitting, not when dragging
				canvas_rect.el().width = 0;
				canvas_rect.el().height = 0;
				canvas_rect.el().style.left = x + "px";
				canvas_rect.el().style.top = y + "px";
				canvas_rect.show();
				break;
			case "crop":
				cropbox.el().style.width = 0;
				cropbox.el().style.height = 0;
				cropbox.el().style.display = "flex";
				cropbox.el().style.left = x + "px";
				cropbox.el().style.top = y + "px";

				//cropbox.el().style.border = "1px dashed "+ color.el().value;
				//cropbox.el().style.color = color.el().value;
        cropbox.el().style.border = "1px dashed "+ my_color;
				cropbox.el().style.color = my_color;
				break;
			case "text":
				// if shown already, loose focus and draw it first, otherwise it gets drawn at mousedown
				if(textbox.hasClass("vjs-hidden")){
					textbox.el().style.width = 0;
					textbox.el().style.height = 0;
					textbox.el().style.left = x + "px";
					textbox.el().style.top = y + "px";
					textbox.show();

					//textbox.el().style.border = "1px dashed "+ color.el().value;
					//textbox.el().style.color = color.el().value;
          textbox.el().style.border = "1px dashed "+ my_color;
					textbox.el().style.color = my_color;
          
					//textbox.el().style.font = size.el().value +"px sans-serif";
          textbox.el().style.font = my_size +"px sans-serif";
// 					textbox.el().style.lineHeight = size.el().value +"px";
				}
				break;
			case "eraser":
				//var s = size.el().value;
        var s = my_size;
				context_draw.clearRect(scale*x - s/2, scale*y - s/2, s, s);
				break;
		}
};
  
	container.on('mousedown', function(e){
		
    
// 		e.preventDefault();
  
  forMouseDown(e);  
	});
  
  
	container.on('mousemove', function(e){
		if(paint){
			var pos = container.el().getBoundingClientRect();
			var x = e.clientX - pos.left;
			var y = e.clientY - pos.top;
			switch(tool){
				case "brush":
					context_draw.lineTo(scale * x, scale * y);
					context_draw.stroke();
					break;
        case "arrow":
          //x = x/scale; y = y/scale; 
          context_arrow.clearRect(0, 0, canvas_arrow.el().width, canvas_arrow.el().height);
					//context_arrow.restore();
				  //var i = Math.sqrt(Math.pow(x0-x, 2));
          //var o = Math.sqrt(Math.pow(y0-y, 2));
          var i = (x-x0); 
          var o = (y-y0); 
          //var i = 200;
          //var o = 100;
          var r, n, l, c, _, h, d, p, u, m;
          //var dist = Math.sqrt((p2[0] - p1[0]) * (p2[0] - p1[0]) + (p2[1] - p1[1]) * (p2[1] - p1[1]));
					l = Math.sqrt(Math.pow(i, 2) + Math.pow(o, 2)); 
					c = l / 6; //head of arrow 
					m = l - c; 
					p = l / 24; //weight of arrow (wide)
					r = Math.atan(1 * o / i * 1); 
					n = Math.cos(r); 
					u = Math.sin(r); 
					_ = Math.cos(Math.PI / 2 - r); 
					h = Math.sin(Math.PI / 2 - r); 
					d = 0 !== i ? i / Math.abs(i) : 1; 
					context_arrow.beginPath(); 
					context_arrow.moveTo(x0, y0); 
					//context_arrow.lineTo(x + d * (m * n - p * _), y + d * (m * u + p * h)); 
          context_arrow.lineTo((x0 + d * (m * n - p * _)), (y0 + d * (m * u + p * h)));
					context_arrow.lineTo(x0 + d * (m * n - 2 * p * _), y0 + d * (m * u + 2 * p * h)); 
					context_arrow.lineTo(x0 + i, y0 + o); 
					context_arrow.lineTo(x0 + d * (m * n + 2 * p * _), y0 + d * (m * u - 2 * p * h)); 
					context_arrow.lineTo(x0 + d * (m * n + p * _), y0 + d * (m * u - p * h));
          //context_draw.closePath();
          //context_arrow.stroke();
          //context_arrow.fillStyle = color.el().value;
          context_arrow.fillStyle = my_color;
          context_arrow.fill();
          //context_arrow.el().style.borderRadius('15px');
          
        
					break;
				case "rect":
					context_rect.clearRect(0, 0, context_rect.canvas.width, context_rect.canvas.height);
					// this way it's only possible to drag to the right and down, mousedown sets top left
					canvas_rect.el().width = x - canvas_rect.el().offsetLeft; // resize canvas
					canvas_rect.el().height = y - canvas_rect.el().offsetTop;
					//context_rect.strokeStyle = color.el().value; //looks like its reset when resizing canvas
          context_rect.strokeStyle = my_color;
					//context_rect.lineWidth = size.el().value / scale; // scale lineWidth
          context_rect.lineWidth = my_size / scale; // scale lineWidth
					context_rect.strokeRect(0, 0, context_rect.canvas.width, context_rect.canvas.height);
					break;
				case "crop":
					cropbox.el().style.width = (x - cropbox.el().offsetLeft) +"px"; // resize
					cropbox.el().style.height = (y - cropbox.el().offsetTop) +"px";
					break;
				case "text":
					textbox.el().style.width = (x - textbox.el().offsetLeft) +"px"; // resize
					textbox.el().style.height = (y - textbox.el().offsetTop) +"px";
					break;
				case "eraser":
					//var s = size.el().value;
          var s = my_size;
					context_draw.clearRect(scale*x - s/2, scale*y - s/2, s, s);
					break;
			}
			e.preventDefault();
		}
	});
	function finish(){
		if(paint){
			paint = false;
			if(tool == "rect"){
				//blit canvas_rect on canvas, scaled
				context_draw.drawImage(canvas_rect.el(),
						scale*canvas_rect.el().offsetLeft, scale*canvas_rect.el().offsetTop,
						scale*context_rect.canvas.width, scale*context_rect.canvas.height);
				canvas_rect.hide();
			}else if(tool == "text"){
				player.el().blur();
				textbox.el().focus();
			} else if(tool == "arrow"){
				//blit canvas_rect on canvas, scaled
				context_draw.drawImage(canvas_arrow.el(),
						scale*canvas_arrow.el().offsetLeft, scale*canvas_arrow.el().offsetTop,
						scale*context_arrow.canvas.width, scale*context_arrow.canvas.height);
				canvas_arrow.hide();
			}
		}
	}
	container.on('mouseup', finish);
	container.on('mouseleave', finish); 
}

videojs.plugin('snapshot', snapshot);


/////////end of 31.12.2015///
////////end of Snapshot functions


var player = videojs('my_video_1');
	player.snapshot();
	

///////////paint button////////
function paint () {
  
player.snap();
  //$(".vjs-marker").css("z-index","1");
  /* 31.01.2016
  $('.vjs-drawing-ctrl').show();
  */
  $('.vjs-canvas-parent').show();
  
  
  $('#submit_btn').show();
  $('#cancel_btn').show();
  $('#description').show();
  $('#time_div').css('display','inline-block');
  //$('#time_div').text(MyVideo.currentTime());
  //MillToTimecode(markers[i].time, 'PAL')
  $('#time_div').text(MillToTimecode(MyVideo.currentTime(), 'PAL'));
  
  //31.01.2016
  
  player.controlBar.progressControl.seekBar.off("mousedown");
  player.controlBar.progressControl.seekBar.off("touchstart");
  player.controlBar.progressControl.seekBar.off("click");
  
  controlShadow.el().style.display = 'block';
  $("#draw_panel").show();
 
  //end 31.01.2016

};


$('#paint_btn').click(function() {
  
  $('#paint_btn').hide(); 
  paint();
  /*
  player.snap();
  
  // 31.01.2016
  
  $('.vjs-canvas-parent').show();
  
  $(this).hide();
  $('#submit_btn').show();
  $('#cancel_btn').show();
  $('#description').show();
  $('#time_div').css('display','inline-block');
  //$('#time_div').text(MyVideo.currentTime());
  //MillToTimecode(markers[i].time, 'PAL')
  $('#time_div').text(MillToTimecode(MyVideo.currentTime(), 'PAL'));
  
  //31.01.2016
  
  player.controlBar.progressControl.seekBar.off("mousedown");
  player.controlBar.progressControl.seekBar.off("touchstart");
  player.controlBar.progressControl.seekBar.off("click");
  
  controlShadow.el().style.display = 'block';
  $("#draw_panel").show();
 
  //end 31.01.2016
  */
  
});
  $('#text_description').val('');

$('#cancel_btn').click(function(){
    
    //$('#paint_btn').show();
    $('#submit_btn').hide();
    $('#cancel_btn').hide();
    $('#description').hide();
    $('#time_div').hide();
		// hide all canvas stuff
		$('.vjs-canvas-parent').hide();
		// switch back to normal player controls
		$('.vjs-drawing-ctrl').hide();
		player.controlBar.show();
		player.el().focus();
    $('#time_div').text('');
    $('#text_description').val('');
  
  
    
  //31.01.2016
  
  player.controlBar.progressControl.seekBar.on("mousedown",  player.controlBar.progressControl.seekBar.handleMouseDown);
  player.controlBar.progressControl.seekBar.on("touchstart", player.controlBar.progressControl.seekBar.handleMouseDown);
  player.controlBar.progressControl.seekBar.on("click", player.controlBar.progressControl.seekBar.handleClick);
  
  //31.01.2016 end
  //01.02.2016
  
  controlShadow.el().style.display = 'none';
  $("#draw_panel").hide();
  //end 01.02.2016
	});


$('#submit_btn').click(function(){
    //var player = videojs('my_video_1');
  
  //31.01.2016
  
  player.controlBar.progressControl.seekBar.on("mousedown",  player.controlBar.progressControl.seekBar.handleMouseDown);
  player.controlBar.progressControl.seekBar.on("touchstart", player.controlBar.progressControl.seekBar.handleMouseDown);
  player.controlBar.progressControl.seekBar.on("click", player.controlBar.progressControl.seekBar.handleClick);
  
  //31.01.2016 end
  
  //01.02.2016
  
  controlShadow.el().style.display = 'none';
  $("#draw_panel").hide();
  
  //end 01.02.2016
  
    //$('#paint_btn').show();
    $('#submit_btn').hide();
    $('#cancel_btn').hide();
    $('#description').hide();  
    $('#time_div').hide();
		// hide all canvas stuff
		$('.vjs-canvas-parent').hide();
		// switch back to normal player controls
		$('.vjs-drawing-ctrl').hide();
		player.controlBar.show();
		player.el().focus();
  
    $('#time_div').text('');
    //$('#text_description').text();
  
    gMarkers = MyVideo.markers.getMarkers();
    //MyVideo.markers.remove([0]);
    /*if (MyVideo.markers) {
      $('#time_div').text(gMarkers.length+'test');
    };*/
    //var marker_flag = 0;
  
    for (var i = 0; i< gMarkers.length; i++) {
      
      if (gMarkers[i].time == MyVideo.currentTime()) {
        
        MyVideo.markers.remove([i]);
        
        //marker_flag = 1;
        
        /*
        MyVideo.markers[i].text = "I'm NEW";
        MyVideo.markers[i].image=combineDrawing("image/png")[2];
        MyVideo.markers[i].image2=combineDrawing("image/png")[0];
        MyVideo.markers[i].image3=combineDrawing("image/png")[1];
        */
        
      }
      
      
    };
    
      MyVideo.markers.add([{
              time: MyVideo.currentTime(),
              //time: MillToTimecode(MyVideo.currentTime(), 'PAL'),
              text: $('#text_description').val(),
              image: combineDrawing("image/png")[2],
              image2: combineDrawing("image/png")[0],
              image3: combineDrawing("image/png")[1]
           }]);
  
    //$('#test').text($('#text_description').val());
    $('#text_description').val('');
   // show markers array
    var markers = MyVideo.markers.getMarkers();
    var minfo = '';
  
     if (markers.length>0) {
    
       for (var i = 0; i < markers.length; i++) {

      //minfo += "<div>"+markers[i].time+"   "+markers[i].text+"</div>";
         minfo += "<div><div style='display:inline-block'>"+(i+1)+".   "+MillToTimecode(markers[i].time, 'PAL')+"</div><div style='display:inline-block; margin-left:20px;'>"+markers[i].text+"</div><div id='keyframe_"+i+"' class='keyframe' style='display:inline-block; margin-left:20px; cursor:pointer'><img src='"+markers[i].image+"' width='100px'/></div></div>";

      }
       $('#marker_div').html(minfo);
       $('.keyframe').click(function() {
         
         var keyframe_marker = markers[$(this).attr('id').substring(9)];
         MyVideo.currentTime(keyframe_marker.time); 
         player.snap(keyframe_marker.image2, keyframe_marker.image3);   
         $('.vjs-canvas-parent').show();
         $('#text_description').val(keyframe_marker.text);
         
       });
     }
    
    /*
    
    player.snap(marker.image2, marker.image3);   
    $('.vjs-canvas-parent').show(); 
    
    */
    //combine function
    //comDr = new MyVideo.snapshot();
   /* 
     var markers = MyVideo.markers.getMarkers();
    $('#div_img').attr('src', markers[0].image);
   */
    //combine function end
 
  
	});

//////////end paint button/////



/////////end 04.01.2016/////////
///////////////////////////////
///////////////////////////////